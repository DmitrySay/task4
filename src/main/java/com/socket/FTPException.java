package com.socket;

import java.io.*;

public class FTPException extends IOException {
    public FTPException(String e) {
        super(e);
    }

    public FTPException() {
    }
}
