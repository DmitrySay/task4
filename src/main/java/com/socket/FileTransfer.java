package com.socket;

import java.util.*;
import java.net.*;
import java.io.*;


public class FileTransfer {

    ClientPI clientPI;

    public FileTransfer(ClientPI pi) {
        this.clientPI = pi;
    }

    public void listDirectoryInPassive(String path) throws IOException {
        // Get the port address of the server DTP
        String[] address = getDataSocketAddress(clientPI.sendCommand("PASV")
                .getResponseText());

        if (address == null) {
            throw new FTPException("Could not obtain socket address");
        }

        String hostName = address[0];
        for (int i = 1; i < 4; i++) {
            hostName += "." + address[i];
        }

        int port = (Integer.parseInt(address[4]) * 256) +
                Integer.parseInt(address[5]);


        // connect to the server DTP
        Socket dataTransferSocket = new Socket(hostName, port);


        // Issue a LIST command
        System.out.println(clientPI.sendCommand("LIST " + path).getResponseText());


        // Download the data from the socket to the
        // console
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                dataTransferSocket.getInputStream()));
        String line = null;

        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        reader.close();
        dataTransferSocket.close();


        // Get the completion reply from the server.
        System.out.println(clientPI.getReply().getResponseText());
    }

    private String[] getDataSocketAddress(String s) {
        System.out.println("Parsing socket address from " + s);
        String[] socketAddressParts = new String[6];
        int beginIdx = s.indexOf('(');
        int endIdx = s.indexOf(')');

        if (beginIdx < 0 ||
                endIdx < 0) {
            return null;
        }

        String addressString = s.substring(
                beginIdx + 1, endIdx);

        StringTokenizer st = new StringTokenizer(addressString, ",");
        for (int i = 0; i < 6; i++) {
            socketAddressParts[i] = st.nextToken();
        }
        return socketAddressParts;
    }

    /**
     * Changes the working directory (like cd). Returns true if successful.
     */
    public synchronized boolean cwd(String dir) throws IOException {
        sendLine("CWD " + dir);
        String response = readLine();
        return (response.startsWith("250 "));
    }

    private String readLine() throws IOException {
        return clientPI.readLine();
    }

    private void sendLine(String line) throws IOException {
        clientPI.sendLine(line);

    }

    public void uploadFileInPassive(String fileName) throws IOException {

        // Set type to binary
        System.out.println(clientPI.sendCommand("TYPE I").getResponseText());

        // Get the port address of the server DTP
        String[] address = getDataSocketAddress(clientPI.sendCommand("PASV")
                .getResponseText());

        if (address == null) {
            throw new FTPException("Could not obtain socket address");
        }

        String hostName = address[0];
        for (int i = 1; i < 4; i++) {
            hostName += "." + address[i];
        }

        int port = (Integer.parseInt(address[4]) * 256) +
                Integer.parseInt(address[5]);


        // connect to the server DTP
        Socket dataTransferSocket = new Socket(hostName, port);


        // Issue a RETR command
        System.out.println(clientPI.sendCommand("STOR " + fileName).getResponseText());


        // Download the data from the socket to the
        // file system (Below is really the client DTP!)
        FileInputStream inputStream = new FileInputStream(fileName);
        OutputStream outputStream = dataTransferSocket.getOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) > -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();

        dataTransferSocket.close();


        // Get the completion reply from the server.
        System.out.println(clientPI.getReply().getResponseText());
    }

    /**
     * Create the working directory .
     */
    public void createDirectory(String directoryName) throws IOException {
        sendLine("MKD " + directoryName);
        String response = readLine();

        if (response.startsWith("257 ")) {
            System.out.println("success" + directoryName);
        } else {
            System.out.println("Fail to create : " + directoryName);
        }
    }

    /**
     * DELETE the working directory .
     */
    public void deleteDirectory(String directoryName) throws IOException {
        sendLine("RMD " + directoryName);
        String response = readLine();

        if (response.startsWith("250 ")) {
            System.out.println("success delete : " + directoryName);
        } else {
            System.out.println("Fail to delete : " + directoryName);
        }
    }

    public void doTask4() throws IOException {
        // Get the port address of the server DTP
        String[] address = getDataSocketAddress(clientPI.sendCommand("PASV")
                .getResponseText());

        if (address == null) {
            throw new FTPException("Could not obtain socket address");
        }

        String hostName = address[0];
        for (int i = 1; i < 4; i++) {
            hostName += "." + address[i];
        }

        int port = (Integer.parseInt(address[4]) * 256) +
                Integer.parseInt(address[5]);


        // connect to the server DTP
        Socket dataTransferSocket = new Socket(hostName, port);

        String path = "/";
        // Issue a LIST command
        System.out.println(clientPI.sendCommand("LIST " + path).getResponseText());


        // Download the data from the socket to the
        // console
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                dataTransferSocket.getInputStream()));
        String line = null;

        List<String> lists = new ArrayList<String>();

        while ((line = reader.readLine()) != null) {
            String firstSimvol = line.substring(0, 1);
            if (firstSimvol.equals("d")) {
                System.out.println(line);
                String[] isbnParts = line.split(" ");
                String pathTodir = isbnParts[isbnParts.length - 1];
                lists.add(pathTodir);
            }
        }


        reader.close();
        dataTransferSocket.close();


        //2
        System.out.println(clientPI.sendCommand("PWD").getResponseText());
        for (String list : lists) {

            clientPI.sendCommand("CWD " + list + "/");
            System.out.println(clientPI.getReply().getResponseText());
            listDirectoryInPassive(" ."); //смотрим список файлов в каждой дериктории

            clientPI.sendCommand("CWD ..");


        }
        System.out.println(clientPI.sendCommand("PWD").getResponseText());


    }


}

