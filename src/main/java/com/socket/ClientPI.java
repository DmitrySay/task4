package com.socket;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.net.*;

public class ClientPI {
    private String hostName;
    private int port;
    private BufferedWriter writer;
    private BufferedReader reader;
    private Socket socket;
    private static boolean DEBUG = false;

    public ClientPI(String hostName, int port) {
        this.hostName = hostName;
        this.port = port;
    }

    /**
     * Connects to an FTP server
     * @throws IOException
     */
    public void open() throws IOException {
        socket = new Socket(hostName, port);
        writer = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream()));
        reader = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
    }


    public FTPResponse sendCommand(String command) throws IOException {
        writer.write(command + "\r\n");
        writer.flush();
        return getReply();
    }


    public FTPResponse getReply() throws IOException {
        StringBuffer sb = new StringBuffer();
        String line = null;
        String resp = null;
        FTPResponse response = null;

        do {
            line = reader.readLine();
            sb.append(line).append("\r\n");
            if (line == null ||
                    line.length() < 3)
                throw new IOException("Illegal com.socket.FTPMain response! " + line);
            if (resp == null)
                resp = line.substring(0, 3);
        }
        while (!(line.startsWith(resp) &&
                line.charAt(3) == ' '));

        if (resp.startsWith("4") || resp.startsWith("5"))
            throw new FTPException("Ftp error! " +
                    resp + ":" + sb);

        return new FTPResponse(resp, sb.toString());
    }

    public void close() throws IOException {
        try {
            if (reader != null) {
                reader.close();
            }

        } catch (FTPException e) {
            System.out.println("reader is not closed");
        }
        try {
            if (writer != null) {
                writer.close();
            }

        } catch (FTPException e) {
            System.out.println("writer is not closed");
        }

        try {
            if (socket != null) {
                socket.close();
            }

        } catch (FTPException e) {
            System.out.println("socket is not closed");
        }

    }

    /**
     * Changes the working directory (like cd). Returns true if successful.
     */
    public synchronized boolean cwd(String dir) throws IOException {
        sendLine("CWD " + dir);
        String response = readLine();
        return (response.startsWith("250 "));
    }

    public String readLine() throws IOException {
        String line = reader.readLine();
        if (DEBUG) {
            System.out.println("< " + line);
        }
        return line;
    }

    /**
     * Sends a raw command to the FTP server.
     */
    public void sendLine(String line) throws IOException {
        if (socket == null) {
            throw new IOException("SimpleFTP is not connected.");
        }
        try {
            writer.write(line + "\r\n");
            writer.flush();
            if (DEBUG) {
                System.out.println("> " + line);
            }
        } catch (IOException e) {
            socket = null;
            throw e;
        }
    }


}




